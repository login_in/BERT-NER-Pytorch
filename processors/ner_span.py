""" Named entity recognition fine-tuning: utilities to work with CoNLL-2003 task. """
import torch
import logging
import re
import os
import copy
import json
from .utils_ner import DataProcessor, get_entities

logger = logging.getLogger(__name__)
space_regex = re.compile(r"^\s*(?P<text>.+?)\s*$")


class InputExample(object):
    """A single training/test example for token classification."""

    def __init__(self, guid, text_a, subject):
        self.guid = guid
        self.text_a = text_a
        self.subject = subject

    def __repr__(self):
        return str(self.to_json_string())

    def to_dict(self):
        """Serializes this instance to a Python dictionary."""
        output = copy.deepcopy(self.__dict__)
        return output

    def to_json_string(self):
        """Serializes this instance to a JSON string."""
        return json.dumps(self.to_dict(), indent=2, sort_keys=True) + "\n"


class InputFeature(object):
    """A single set of features of data."""

    def __init__(self, input_ids, input_mask, input_len, segment_ids, start_ids, end_ids, subjects):
        self.input_ids = input_ids
        self.input_mask = input_mask
        self.segment_ids = segment_ids
        self.start_ids = start_ids
        self.input_len = input_len
        self.end_ids = end_ids
        self.subjects = subjects

    def __repr__(self):
        return str(self.to_json_string())

    def to_dict(self):
        """Serializes this instance to a Python dictionary."""
        output = copy.deepcopy(self.__dict__)
        return output

    def to_json_string(self):
        """Serializes this instance to a JSON string."""
        return json.dumps(self.to_dict(), indent=2, sort_keys=True) + "\n"


def collate_fn(batch):
    """
    batch should be a list of (sequence, target, length) tuples...
    Returns a padded tensor of sequences sorted from longest to shortest,
    """
    all_input_ids, all_input_mask, all_segment_ids, all_start_ids, all_end_ids, all_lens = map(torch.stack, zip(*batch))
    max_len = max(all_lens).item()
    all_input_ids = all_input_ids[:, :max_len]
    all_input_mask = all_input_mask[:, :max_len]
    all_segment_ids = all_segment_ids[:, :max_len]
    all_start_ids = all_start_ids[:, :max_len]
    all_end_ids = all_end_ids[:, :max_len]
    return all_input_ids, all_input_mask, all_segment_ids, all_start_ids, all_end_ids, all_lens


def convert_examples_to_features(examples, label_list, max_seq_length, tokenizer,
                                 cls_token_at_end=False, cls_token="[CLS]", cls_token_segment_id=1,
                                 sep_token="[SEP]", pad_on_left=False, pad_token=0, pad_token_segment_id=0,
                                 sequence_a_segment_id=0, mask_padding_with_zero=True, ):
    """ Loads a data file into a list of `InputBatch`s
        `cls_token_at_end` define the location of the CLS token:
            - False (Default, BERT/XLM pattern): [CLS] + A + [SEP] + B + [SEP]
            - True (XLNet/GPT pattern): A + [SEP] + B + [SEP] + [CLS]
        `cls_token_segment_id` define the segment id associated to the CLS token (0 for BERT, 2 for XLNet)
    """
    label2id = {label: i for i, label in enumerate(label_list)}
    features = []
    for (ex_index, example) in enumerate(examples):
        if ex_index % 10000 == 0:
            logger.info("Writing example %d of %d", ex_index, len(examples))
        textlist = example.text_a
        subjects = example.subject
        if isinstance(textlist, list):
            textlist = " ".join(textlist)
        tokens = tokenizer.tokenize(textlist)
        start_ids = [0] * len(tokens)
        end_ids = [0] * len(tokens)
        subjects_id = []
        for subject in subjects:
            label = subject[0]
            start = subject[1]
            end = subject[2]
            start_ids[start] = label2id[label]
            end_ids[end] = label2id[label]
            subjects_id.append((label2id[label], start, end))
        # Account for [CLS] and [SEP] with "- 2".
        special_tokens_count = 2
        if len(tokens) > max_seq_length - special_tokens_count:
            tokens = tokens[: (max_seq_length - special_tokens_count)]
            start_ids = start_ids[: (max_seq_length - special_tokens_count)]
            end_ids = end_ids[: (max_seq_length - special_tokens_count)]
        # The convention in BERT is:
        # (a) For sequence pairs:
        #  tokens:   [CLS] is this jack ##son ##ville ? [SEP] no it is not . [SEP]
        #  type_ids:   0   0  0    0    0     0       0   0   1  1  1  1   1   1
        # (b) For single sequences:
        #  tokens:   [CLS] the dog is hairy . [SEP]
        #  type_ids:   0   0   0   0  0     0   0
        #
        # Where "type_ids" are used to indicate whether this is the first
        # sequence or the second sequence. The embedding vectors for `type=0` and
        # `type=1` were learned during pre-training and are added to the wordpiece
        # embedding vector (and position vector). This is not *strictly* necessary
        # since the [SEP] token unambiguously separates the sequences, but it makes
        # it easier for the model to learn the concept of sequences.
        #
        # For classification tasks, the first vector (corresponding to [CLS]) is
        # used as as the "sentence vector". Note that this only makes sense because
        # the entire model is fine-tuned.
        tokens += [sep_token]
        start_ids += [0]
        end_ids += [0]
        segment_ids = [sequence_a_segment_id] * len(tokens)
        if cls_token_at_end:
            tokens += [cls_token]
            start_ids += [0]
            end_ids += [0]
            segment_ids += [cls_token_segment_id]
        else:
            tokens = [cls_token] + tokens
            start_ids = [0] + start_ids
            end_ids = [0] + end_ids
            segment_ids = [cls_token_segment_id] + segment_ids

        input_ids = tokenizer.convert_tokens_to_ids(tokens)
        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        input_mask = [1 if mask_padding_with_zero else 0] * len(input_ids)
        input_len = len(input_ids)
        # Zero-pad up to the sequence length.
        padding_length = max_seq_length - len(input_ids)
        if pad_on_left:
            input_ids = ([pad_token] * padding_length) + input_ids
            input_mask = ([0 if mask_padding_with_zero else 1] * padding_length) + input_mask
            segment_ids = ([pad_token_segment_id] * padding_length) + segment_ids
            start_ids = ([0] * padding_length) + start_ids
            end_ids = ([0] * padding_length) + end_ids
        else:
            input_ids += [pad_token] * padding_length
            input_mask += [0 if mask_padding_with_zero else 1] * padding_length
            segment_ids += [pad_token_segment_id] * padding_length
            start_ids += ([0] * padding_length)
            end_ids += ([0] * padding_length)

        assert len(input_ids) == max_seq_length
        assert len(input_mask) == max_seq_length
        assert len(segment_ids) == max_seq_length
        assert len(start_ids) == max_seq_length
        assert len(end_ids) == max_seq_length

        if ex_index < 5:
            logger.info("*** Example ***")
            logger.info("guid: %s", example.guid)
            logger.info("tokens: %s", " ".join([str(x) for x in tokens]))
            logger.info("input_ids: %s", " ".join([str(x) for x in input_ids]))
            logger.info("input_mask: %s", " ".join([str(x) for x in input_mask]))
            logger.info("segment_ids: %s", " ".join([str(x) for x in segment_ids]))
            logger.info("start_ids: %s" % " ".join([str(x) for x in start_ids]))
            logger.info("end_ids: %s" % " ".join([str(x) for x in end_ids]))

        features.append(InputFeature(input_ids=input_ids,
                                     input_mask=input_mask,
                                     segment_ids=segment_ids,
                                     start_ids=start_ids,
                                     end_ids=end_ids,
                                     subjects=subjects_id,
                                     input_len=input_len))
    return features


class CnerProcessor(DataProcessor):
    """Processor for the chinese ner data set."""

    def get_train_examples(self, data_dir):
        """See base class."""
        return self._create_examples(self._read_text(os.path.join(data_dir, "train.char.bmes")), "train")

    def get_dev_examples(self, data_dir):
        """See base class."""
        return self._create_examples(self._read_text(os.path.join(data_dir, "dev.char.bmes")), "dev")

    def get_test_examples(self, data_dir):
        """See base class."""
        return self._create_examples(self._read_text(os.path.join(data_dir, "test.char.bmes")), "test")

    def get_labels(self):
        """See base class."""
        return ["O", "CONT", "ORG", "LOC", 'EDU', 'NAME', 'PRO', 'RACE', 'TITLE']

    def _create_examples(self, lines, set_type):
        """Creates examples for the training and dev sets."""
        examples = []
        for (i, line) in enumerate(lines):
            if i == 0:
                continue
            guid = "%s-%s" % (set_type, i)
            text_a = line['words']
            labels = []
            for x in line['labels']:
                if 'M-' in x:
                    labels.append(x.replace('M-', 'I-'))
                elif 'E-' in x:
                    labels.append(x.replace('E-', 'I-'))
                else:
                    labels.append(x)
            subject = get_entities(labels, id2label=None, markup='bios')
            examples.append(InputExample(guid=guid, text_a=text_a, subject=subject))
        return examples


class CluenerProcessor(DataProcessor):
    """Processor for the chinese ner data set."""

    def get_train_examples(self, data_dir):
        """See base class."""
        return self._create_examples(self._read_json(os.path.join(data_dir, "train.json")), "train")

    def get_dev_examples(self, data_dir):
        """See base class."""
        return self._create_examples(self._read_json(os.path.join(data_dir, "dev.json")), "dev")

    def get_test_examples(self, data_dir):
        """See base class."""
        return self._create_examples(self._read_json(os.path.join(data_dir, "test.json")), "test")

    def get_labels(self):
        """See base class."""
        return ["O", "address", "book", "company", 'game', 'government', 'movie', 'name', 'organization', 'position',
                'scene']

    def _create_examples(self, lines, set_type):
        """Creates examples for the training and dev sets."""
        examples = []
        for (i, line) in enumerate(lines):
            guid = "%s-%s" % (set_type, i)
            text_a = line['words']
            labels = line['labels']
            subject = get_entities(labels, id2label=None, markup='bios')
            examples.append(InputExample(guid=guid, text_a=text_a, subject=subject))
        return examples


def convert_examples_to_features2(examples, label_list, max_seq_length, tokenizer):
    """
    use tokenizer.encode_plus to convert data
    Args:
        examples:
        label_list:
        max_seq_length:
        tokenizer:

    Returns:

    """
    label_map = {label: i for i, label in enumerate(label_list)}
    features = []
    for (ex_index, example) in enumerate(examples):
        inputs = tokenizer.encode_plus(example.text_a,
                                       add_special_tokens=True,
                                       max_length=max_seq_length,
                                       padding="max_length",
                                       truncation=True,
                                       return_offsets_mapping=True)
        char2token_offset_map = {
            char_offset: token_offset
            for token_offset, (char_start_offset, char_end_offset) in enumerate(inputs["offset_mapping"])
            for char_offset in range(char_start_offset, char_end_offset)
        }
        start_ids = [0] * len(inputs["input_ids"])
        end_ids = [0] * len(inputs["input_ids"])
        subjects_id = []
        for entity_dict in example.subject:
            # 防止标注实体前后有空格，导致后期分词出现bug
            match = space_regex.match(entity_dict['entity'])
            if match:
                match_start, match_end = match.start("text"), match.end("text")
                entity_dict["end_idx"] = entity_dict["start_idx"] + match_end - 1
                entity_dict["start_idx"] += match_start
                entity_dict["entity"] = match.group("text")
            char_s, char_e, tag = entity_dict["start_idx"], entity_dict["end_idx"], entity_dict["type"]
            if char_s in char2token_offset_map and char_e in char2token_offset_map:
                tok_s = char2token_offset_map[char_s]
                tok_e = char2token_offset_map[char_e]
                start_ids[tok_s] = label_map[tag]
                end_ids[tok_e] = label_map[tag]
                subjects_id.append((label_map[tag], tok_s - 1, tok_e - 1))  # -1 for [CLS]

        input_len = sum(inputs['attention_mask'])
        features.append(InputFeature(input_ids=inputs["input_ids"],
                                     input_mask=inputs["attention_mask"],
                                     input_len=input_len,
                                     segment_ids=inputs.get("token_type_ids", inputs["attention_mask"]),
                                     start_ids=start_ids,
                                     end_ids=end_ids,
                                     subjects=subjects_id))

    return features


class EMRImageReportProcessor(DataProcessor):
    original_labels = ["部位", "疾病", "症状", "检测项目", "数值", "病灶", "修饰词", "检查名称", "指代词", "未见疾病"]

    @staticmethod
    def _get_examples(data_dir, mode):
        examples = []
        with open(os.path.join(data_dir, f"{mode}.json")) as f:
            for i, item in enumerate(json.load(f)):
                text_a = item['text']
                guid = "%s-%s" % (mode, i)
                labels = item["entities"]

                examples.append(InputExample(guid=guid, text_a=text_a, subject=labels))

        return examples

    def get_train_examples(self, data_dir):
        """See base class."""
        return self._get_examples(data_dir, "train")

    def get_dev_examples(self, data_dir):
        """See base class."""
        return self._get_examples(data_dir, "dev")

    def get_test_examples(self, data_dir):
        """See base class."""
        return self._get_examples(data_dir, "test")

    def get_labels(self):
        """See base class."""
        return ["O"] + self.original_labels


class EMRNerProcessor(EMRImageReportProcessor):
    original_labels = ['TIME', 'SYMPTOM', 'RIS', 'SURGERY', 'DISEASE', 'LIS_ITEMS', 'DRUG', 'PE_ITEMS', 'LIS', 'UNIT',
                       'THERAPY', 'CHEMO_PLAN', 'GENE']


class ETMFProcessor(EMRImageReportProcessor):
    original_labels = ['中心名称', '中心ID', '主要研究者', '版本号', '版本日期', '生效日期', '有效期']


ner_processors = {
    "cner": CnerProcessor,
    'cluener': CluenerProcessor,
    'emr-image-report': EMRImageReportProcessor,
    'emr_v3': EMRNerProcessor,
    'etmf': ETMFProcessor,
}
